## odooapi

### odooapi 介绍

1. 继承 odoorpc, 包含 odoorpc 的所有功能
2. 封装 Action, Views, Node
3. 通过 Action.load 和 Action.load_views 接口获取 Action 及 Views 信息
4. Action 对应 odoo 的 action
5. 基于 Action 及 Views 信息, 取得 页面结构
6. 基于 Action 及 Views 信息, 调接口获取数据
7. 渲染页面和数据

### 继承 odoorpc, 包含 odoorpc 的所有功能

1. 导入 odooapi: import api from '@/odooapi'. 等同于 导入 odoorpc. 可以使用 odoorpc 所有功能
2. 初始化 api.init({baseURL, timeout}) 等同于初始化 odoorpc
3. 登录接口: api.web.login
4. 模型接口: Model = api.env.model(model_name)
5. 调用模型方法: Model.search(domain, {limit:10})
6. 获取 context: api.web.session.context

### Action 接口

### Views 接口

### one2many
