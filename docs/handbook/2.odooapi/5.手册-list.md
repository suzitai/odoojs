### list view

#### 获取 action 及 views 信息

1. list view 展示数据. 首先调取接口, 获得 action 及 views 信息
2. 该信息, 暂存在页面中
3. 后续页面操作, 调取数据接口, 将该信息作为参数.

```
import api from '@/odooapi'
const info_get = async () {
    const action_xml_id = 'contacts.action_contacts'
    const action = await api.Action.load(action_id)
    const views = await api.Action.load_views({action})
    const context = api.web.session.context
    const info = { context, action, views }
    return info
}

```

#### 编辑搜索条件

1. 搜索条件用于对数据的过滤
2. 搜索条件, 暂存在页面中
3. 可对搜索条件进行编辑
4. 后续, 调取 获取数据接口, 将搜索条件作为参数

```
import api from '@/odooapi'
const search_edit = async () {
    const info = {context, action, views}

    // 获取 默认搜索条件
    let search = await api.Views.search.default_value(info)

    // 搜索条件转换为搜索框数据显示格式
    const search_display = api.Views.search.display_value(info, search)

    // 筛选下拉菜单选择项
    const filter_options = api.Views.search.filter_options(info, search)

    // 分组下拉菜单选择项
    const groupby_options = api.Views.search.groupby_options(info, search)

    // 搜索框下拉 选择项
    const filters_options = api.Views.search.search_options(info, search)

    // 收藏按钮下拉菜单选择项
    const filters_options = api.Views.search.filters_options(info, search)

    // 编辑搜索条件
    search = api.Views.search(info, search,
    {name: 'search_item_name', value: true})

    return search
}

```

#### 查询 list view 的数据

1. list view 展示数据. 调取接口, 获取数据.
2. 接口: api.Views.list.load_data
3. 参数: {context, action, views}, { pagination, search}
   {context, action, views} 为 action/views 信息.
   pagination 为分页数据.
   search 搜索条件.
4. 页面中已有 {context, action, views}, { pagination, search}
5. 返回 {length, records, groups}
   若搜索条件中无分组要求. 返回 { length, records}
   若搜索条件中有分组要求. 返回 { length, groups}

6. 该接口底层是调用的 Model.web_search_read 或 Model.web_read_group.
   Model.web_search_read 或 Model.web_read_group 接口需要的参数:
   domain, groupby, fields.
   这些参数, 由 context, action, views, pagination, search 中获取.

```
import api from '@/odooapi'

const test_list_view = async () {
    const info = {context, action, views}
    const search = {search_item_name1: 1, search_item_name2: 1, }
    const data = await api.Views.list.load_data(
        info,
        { pagination, search }
    )
    console.log('data', data)
}

```

#### 展示列表数据

1. 获取数据之后, 将数据在页面中展示.

```
import api from '@/odooapi'
const info = {context, action, views}

// fields 信息
const fields = views.list.fields

// node 信息
const node = api.Views.list.view_node(info)

```
