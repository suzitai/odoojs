export const menus = [
  { name: 'sale', icon: 'shopping', theme: 'twoTone', title: '销售' },
  { name: 'purchase', icon: 'shopping-cart', title: '采购' },
  { name: 'cash', icon: 'pay-circle', title: '出纳' },
  { name: 'finance', icon: 'account-book', theme: 'twoTone', title: '会计' },
  { name: 'stock', icon: 'database', theme: 'twoTone', title: '库存' },
  { name: 'mrp', icon: 'setting', theme: 'twoTone', title: '生产' }
]

// <!-- <a-icon type="shop" /> -->

// <!-- <a-icon type="home" />
// <a-icon type="video-camera" /> -->

// <!-- <a-menu-item key="_base.action_partner_category_form">
// <span>参与人类型</span>
// </a-menu-item>

// <a-menu-item key="_contacts.action_contacts">
// <span>参与人</span>
// </a-menu-item>

// <a-menu-item key="_sale.action_sale_home">
// <a-icon type="video-camera" />
// <span>销售</span>
// </a-menu-item> -->
